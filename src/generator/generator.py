import json
import random
import time


class Location:
    def __init__(self, latitude, longitude):
        self.latitude = latitude
        self.longitude = longitude

class Lightning:
    def __init__(self, pointInTime): #point in time - second expired from begining of simulation
        self.pointInTime = pointInTime
        self.location = Location(0,0)

    def getKey(self):
        return self.pointInTime

    def __repr__(self):
        return 'Lat : ' + str(self.location.latitude) + ' Long : ' + str(self.location.longitude) + ' time ' + str(self.pointInTime) + '\n'

class Thunderstorm:
    def __init__(self, startPoint, direct, speedLatitude, speedLongitude, lightningInterval, scale, timeToLive):
        self.startPoint = startPoint
        self.direct = direct
        self.speedLatitude = speedLatitude            # speed in geo-minutes per second
        self.speedLongitude = speedLongitude            # speed in geo-minutes per second
        self.lightningInterval = lightningInterval
        self.scale = scale
        self.timeToLive = timeToLive

    def resolve(self):
        lightnings = self.generateLightningTimes()
        lightnings = self.generateLightningLocations(lightnings)

        return lightnings

    def generateLightningTimes(self):
        lightning = []
        count = 0
        while count <= self.timeToLive :
            time = count + random.normalvariate(0, self.lightningInterval)
            lightning.append(Lightning(time))
            count = count+self.lightningInterval


        return lightning

    def generateLightningLocations(self,lightnings):

        for lightning in lightnings:
            time = lightning.pointInTime

            latitudeInterwalOfStormCore = self.speedLatitude * time     # Y axis
            longitudeInterwalOfStormCore = self.speedLongitude * time   # X axis

            if self.direct > 0 & self.direct<=90:
                stormCoreLatitudePostion = self.startPoint.latitude + latitudeInterwalOfStormCore
                stormCoreLongitudePosition = self.startPoint.longitude + longitudeInterwalOfStormCore

            elif self.direct > 90 & self.direct<=180:
                stormCoreLatitudePostion = self.startPoint.latitude + latitudeInterwalOfStormCore
                stormCoreLongitudePosition = self.startPoint.longitude - longitudeInterwalOfStormCore

            elif self.direct >180  & self.direct<=270:
                stormCoreLatitudePostion = self.startPoint.latitude - latitudeInterwalOfStormCore
                stormCoreLongitudePosition = self.startPoint.longitude - longitudeInterwalOfStormCore

            elif self.direct > 270  & self.direct<360:
                stormCoreLatitudePostion = self.startPoint.latitude - latitudeInterwalOfStormCore
                stormCoreLongitudePosition = self.startPoint.longitude + longitudeInterwalOfStormCore

            stormCore = Location(stormCoreLatitudePostion, stormCoreLongitudePosition)

            lightningLatitudeDeviation = stormCore.latitude + \
                                         random.normalvariate(0, self.scale)
            lightningLongitudeDeviation = stormCore.longitude + \
                                          random.normalvariate(0, self.scale)

            lightning.location = Location(lightningLatitudeDeviation, lightningLongitudeDeviation)

        return lightnings

class Loader:
    def __init__(self, path):
        self.path = path

    def loadFileAndGetContent(self):
        self.file = open(self.path)

        try:
            content = self.file.read()
            interprete_content = self.__interpreteContent(content)
        finally:
            self.file.close()

        return interprete_content


    def __interpreteContent(self, content):
        stormsObjects =[]

        content = json.loads(content)
        for storm in content['Storms']:
            stormsObjects.append(self.__createStromObject(storm['Storm']))

        return stormsObjects


    def __createStromObject(self, stormData):

        startLocation = stormData['startPoint']
        startPoint = Location(startLocation['latitude'], startLocation['longitude'])


        return Thunderstorm(startPoint, stormData['direct'],stormData['speedLatitude'], stormData['speedLongitude'],
                            stormData['lightningInterval'], stormData['scale'],stormData['TTL'])


class Generator:
    def __init__(self, sourceFilePath ):
        loader = Loader(sourceFilePath)
        self.content = loader.loadFileAndGetContent()

        self.maxOfTimeToLive = 0
        self.lightnings =[]

        for storm in self.content:
            self.lightnings.extend(storm.resolve())
            if storm.timeToLive > self.maxOfTimeToLive:
                self.maxOfTimeToLive = storm.timeToLive

    def generate(self, periodOfTime):
        self.lightnings = sorted(self.lightnings, key= lambda lightning : lightning.pointInTime, reverse=True)

        downLimit = -1
        for i in range(5,self.maxOfTimeToLive+periodOfTime,periodOfTime):
            self.lightningsForRelease = []
            while len(self.lightnings) > 0:
                if (self.lightnings[-1].pointInTime > downLimit) & (self.lightnings[-1].pointInTime < i) :
                    self.lightningsForRelease.append(self.lightnings.pop())
                else :
                    break
            downLimit = i

            time.sleep(periodOfTime)
            print(self.lightningsForRelease)


generator = Generator("./storms.txt")
generator.generate(5)



