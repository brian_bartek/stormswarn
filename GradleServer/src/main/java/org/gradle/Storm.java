package org.gradle;

public class Storm {
	
	private final String id;
	private final String latitude;
	private final String longtitude;
	private final String scale;
	private final String azimuth;
	
	public Storm(String id, String lat, String longt, String scale, String azimuth) {
		this.id = id;
		this.latitude = lat;
		this.longtitude = longt;
		this.scale = scale;
		this.azimuth = azimuth;
	}
	
	public String getId() {
		return id;
	}
	public String getLatitude() {
		return latitude;
	}
	public String getLongtitude() {
		return longtitude;
	}
	public String getScale() {
		return scale;
	}
	public String getAzimuth() {
		return azimuth;
	}
}
