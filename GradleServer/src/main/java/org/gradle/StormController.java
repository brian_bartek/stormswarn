package org.gradle;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/storm")
public class StormController {

	private final AtomicLong counter = new AtomicLong();

	@RequestMapping( method = RequestMethod.GET )
	public Storm stormSet(@RequestParam(value="longtitude", defaultValue="12") String longtitude,
			   @RequestParam(value="latitude", defaultValue="12") String latitude,
			   @RequestParam(value="scale", defaultValue="12") String scale,
			   @RequestParam(value="azimuth", defaultValue="12") String azimuth) {
		return new Storm(Long.toString(counter.incrementAndGet()), latitude, longtitude, scale, azimuth);
	}
	
	@RequestMapping( method = RequestMethod.GET )
	public Storm storm() {
		StormServer stormServer;
		String latitude = null;
		String longtitude = null;
		String scale = null;
		String azimuth = null;
		return new Storm(Long.toString(counter.incrementAndGet()), latitude, longtitude, scale, azimuth);
	}
}
