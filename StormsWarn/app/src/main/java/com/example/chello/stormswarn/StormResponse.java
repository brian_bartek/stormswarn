package com.example.chello.stormswarn;

/**
 * Created by Chello on 2015-06-08.
 */
public class StormResponse {

    private String id;
    private String latitude;
    private String longtitude;
    private String scale;
    private String azimuth;

    public StormResponse(String id, String lat, String longt, String scale, String azimuth) {
        this.id = id;
        this.latitude = lat;
        this.longtitude = longt;
        this.scale = scale;
        this.azimuth = azimuth;
    }

    public String getId() {
        return id;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public String getScale() {
        return scale;
    }

    public String getAzimuth() {
        return azimuth;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLat(String lat) {
        this.latitude = lat;
    }

    public void setLongt(String longt) {
        this.longtitude = longt;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    public void setAzimuth(String azimuth) {
        this.azimuth = azimuth;
    }
}
