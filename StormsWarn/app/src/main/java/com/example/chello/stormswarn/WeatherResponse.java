package com.example.chello.stormswarn;

import java.util.List;

/**
 * Created by Chello on 2015-05-26.
 */
public class WeatherResponse {
    private WeatherCoord coord;
    private String base;
    private List<Weather> weather;

    public WeatherResponse(WeatherCoord coord, String base, String description, List<Weather> weather) {
        this.coord = coord;
        this.base = base;
        this.weather = weather;
    }

    public WeatherCoord getCoord() {
        return coord;
    }

    public void setCoord(WeatherCoord coord) {
        this.coord = coord;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> main) {
        this.weather = main;
    }

}
