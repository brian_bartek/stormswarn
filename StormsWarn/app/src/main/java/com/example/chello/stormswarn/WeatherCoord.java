package com.example.chello.stormswarn;

/**
 * Created by Chello on 2015-06-09.
 */
public class WeatherCoord {
    private double lon;
    private double lat;

    public WeatherCoord(double lon, double lat) {
        this.lon = lon;
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
