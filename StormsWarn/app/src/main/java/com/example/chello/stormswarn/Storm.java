package com.example.chello.stormswarn;

/**
 * Created by Chello on 2015-06-08.
 */
public class Storm {

    private String id;
    private String lat;
    private String longt;
    private String scale;
    private String azimuth;

    public Storm(String id, String lat, String longt, String scale, String azimuth) {
        this.id = id;
        this.lat = lat;
        this.longt = longt;
        this.scale = scale;
        this.azimuth = azimuth;
    }

    public String getId() {
        return id;
    }

    public String getLat() {
        return lat;
    }

    public String getLongt() {
        return longt;
    }

    public String getScale() {
        return scale;
    }

    public String getAzimuth() {
        return azimuth;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void setLongt(String longt) {
        this.longt = longt;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    public void setAzimuth(String azimuth) {
        this.azimuth = azimuth;
    }
}
