package com.example.chello.stormswarn;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface RestApi {

    @GET("/weather")
    void getWeather(@Query("q") String cityName, Callback<WeatherResponse> callback);

    @GET("/storm")
    void getStorm(@Query("q") String storm, Callback<StormResponse> callback);
}
